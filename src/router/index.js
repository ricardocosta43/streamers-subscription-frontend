import Vue from "vue";
import VueRouter from 'vue-router'
import Home from "@/views/Home"
import LoginClient from "@/views/client/LoginClient"
import SignupClient from "@/views/client/SignupClient"
import DashboardClient from "@/views/client/DashboardClient"
import LoginStreamer from "@/views/streamer/LoginStreamer"
import SignupStreamer from "@/views/streamer/SignupStreamer"
import DashboardStreamer from "@/views/streamer/DashboardStreamer"
import PageNotFound from "@/views/NotFound"

Vue.use(VueRouter)

const routes = [
    {
        name: 'HomePage',
        path: '/',
        component: Home,
    },
    {
        name: 'LoginClient',
        path: '/loginClient',
        component: LoginClient,
    },
    {
        name: 'SignupClient',
        path: '/signupClient',
        component: SignupClient,
    },
    {
        name: 'DashboardClient',
        path: '/dashboardClient',
        component: DashboardClient,
    },
    {
        name: 'LoginStreamer',
        path: '/loginStreamer',
        component: LoginStreamer,
    },
    {
        name: 'SignupStreamer',
        path: '/signupStreamer',
        component: SignupStreamer,
    },
    {
        name: 'DashboardStreamer',
        path: '/dashboardStreamer',
        component: DashboardStreamer,
    },
    { path: "*", component: PageNotFound }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router