export default {
    namespaced: true,
    state: {
        token: null,
        user: null
    },
    actions: {
        token({ commit }, token) {
            commit('SET_TOKEN', token)
        },
        user({ commit }, user) {
            commit('SET_USER', user)
        }
    },

    mutations: {
        SET_TOKEN(state, token) {
            state.token = token
        },
        SET_USER(state, user) {
            state.user = user
        }
    },    
};