export default {
    namespaced: true,
    state: {
        header: null
    },
    actions: {
        header({ commit }, header) {
            commit('SET_HEADER', header)
        },
       
    },

    mutations: {
        SET_HEADER(state, header) {
            state.header = header
        },
        
    },    
};