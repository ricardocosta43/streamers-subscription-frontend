import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import headers from './headers'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
  },
  mutations: {

  },
  actions: {

  },
  getters: {

  },
  modules: {
    auth,
    headers
  }
})